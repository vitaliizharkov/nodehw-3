const mongoose = require('mongoose');

const loadSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  payload: {
    type: Number,
    required: true,
  },
  createdDate: {
    type: Date,
    default: Date.now(),
  },
  created_by: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: 'User',
  },
  pickup_address: {
    type: String,
    required: true,
  },
  delivery_address: {
    type: String,
    required: true,
  },
  dimensions: {
    width: {
      type: Number,
      required: true,
    },
    length: {
      type: Number,
      required: true,
    },
    height: {
      type: Number,
      required: true,
    },
  },
  logs: [
    {
      message: {
        type: String,
      },
      time: {
        type: Date,
        default: Date.now(),
      },
    },
  ],
  status: {
    type: String,
    default: 'NEW',
  },
  state: {
    type: String,
    default: 'En route to Pick Up',
  },
  assigned_to: {
    type: mongoose.Schema.Types.ObjectId,
    default: null,
    ref: 'User',
  },
},
{collection: 'loads'});

module.exports.Load = mongoose.model('Load', loadSchema);
