const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
  email: {
    type: String,
    required: true,
    uniq: true,
  },
  password: {
    type: String,
    required: true,
  },
  createdDate: {
    type: Date,
    default: Date.now(),
  },
  role: {
    type: String,
    required: true,
  },
});

userSchema.virtual('trucks', {
  ref: 'Truck',
  localField: '_id',
  foreignField: 'created_by',
});

userSchema.virtual('loads', {
  ref: 'Load',
  localField: '_id',
  foreignField: 'created_by',
});

module.exports.User = mongoose.model('User', userSchema);
