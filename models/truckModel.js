const mongoose = require('mongoose');

const truckSchema = new mongoose.Schema(
    {
      created_by: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: 'User',
      },
      assigned_to: {
        type: mongoose.Schema.Types.ObjectId,
        default: null,
        ref: 'User',
      },
      type: {
        type: String,
        required: true,
        trim: true,
      },
      status: {
        type: String,
        default: 'IS',
        trim: true,
      },
      created_date: {
        type: Date,
        default: Date.now(),
      },
    },
    {
      collection: 'trucks',
    },
);

module.exports.Truck = mongoose.model('Truck', truckSchema);
