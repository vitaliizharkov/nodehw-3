const express = require('express');
const router = new express.Router();

const {asyncWrapper} = require('./helpers');
const {validateRegistration} = require('../middlewares/validation');
const {login, registration} = require('../controllers/authController');

router.post('/register',
    asyncWrapper(validateRegistration), asyncWrapper(registration));
router.post('/login', asyncWrapper(login));

module.exports = router;
