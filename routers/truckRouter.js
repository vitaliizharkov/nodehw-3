const express = require('express');
const router = new express.Router();

const {authMiddleware} = require('../middlewares/authMiddleware');
const {asyncWrapper} = require('./helpers');
const {getTruck,
  addTruck,
  getTruckById,
  updateTruckById,
  assignTruck,
  deleteTruck} = require('../controllers/truckController');


router.get('', authMiddleware, asyncWrapper(getTruck));
router.post('', authMiddleware, asyncWrapper(addTruck));
router.get('/:id', authMiddleware, asyncWrapper(getTruckById));
router.put('/:id', authMiddleware, asyncWrapper(updateTruckById));
router.delete('/:id', authMiddleware, asyncWrapper(deleteTruck));
router.post('/:id/assign', authMiddleware, asyncWrapper(assignTruck));

module.exports = router;
