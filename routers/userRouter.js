const express = require('express');
const router = new express.Router();

const {authMiddleware} = require('../middlewares/authMiddleware');
const {getUserInfo, deleteUser,
  changeUserPassword} = require('../controllers/userController');

router.get('/me', authMiddleware, getUserInfo);
router.delete('/me', authMiddleware, deleteUser);
router.patch('/me', authMiddleware, changeUserPassword);

module.exports = router;
