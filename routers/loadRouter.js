const express = require('express');
const router = new express.Router();

const {asyncWrapper} = require('./helpers');
const {authMiddleware} = require('../middlewares/authMiddleware');
const {createLoad,
  getLoads,
  getLoadById,
  iterateLoadState,
  updateLoad,
  deleteLoad,
  postLoad,
  getShippingInfo} = require('../controllers/loadController');

router.post('', authMiddleware, asyncWrapper(createLoad));
router.get('', authMiddleware, asyncWrapper(getLoads));
router.get('/:id', authMiddleware, asyncWrapper(getLoadById));
router.put('/:id', authMiddleware, asyncWrapper(updateLoad));
router.delete('/:id', authMiddleware, asyncWrapper(deleteLoad));
router.patch('/:id', authMiddleware, asyncWrapper(iterateLoadState));
router.post('/:id/post', authMiddleware, asyncWrapper(postLoad));
router.get('/:id/shipping_info', authMiddleware, asyncWrapper(getShippingInfo));

module.exports = router;
