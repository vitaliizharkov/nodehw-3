const {Load} = require('../models/loadModel');
const {User} = require('../models/userModel.js');
const {Truck} = require('../models/truckModel');

const createLoad = async (req, res) => {
  const user = await User.findOne({_id: req.user._id});
  console.log(user, user.role);
  if (!user) {
    return res.status(400).json({message: 'User are not found!'});
  }

  if (user.role === 'SHIPPER') {
    const {name, payload, pickup_address, delivery_address,
      dimensions: {width, length, height}} = req.body;
    if (name &&
      payload &&
      pickup_address &&
      delivery_address &&
      width &&
      length &&
      height) {
      const load = new Load({
        created_by: req.user._id,
        name,
        payload,
        pickup_address,
        delivery_address,
        dimensions: {
          width,
          length,
          height,
        },
      });
      await load.save();
      res.status(200).json({message: 'Load created successfully'});
    } else {
      return res.status(400).json({message: 'Enter valid data'});
    }
  } else {
    return res.status(400).json({message: 'You are not the shipper!'});
  }
};

const getLoads = async (req, res) => {
  const user = await User.findOne({_id: req.user._id});

  if (!user) {
    return res.status(400).json({message: 'User are not found!'});
  }

  if (user.role === 'SHIPPER') {
    const match = {};
    if (req.query.status) {
      match.status = JSON.parse(req.query.status);
    }
    try {
      const loads = await Load.find({created_by: req.user._id})
      await user
          .populate({
            path: 'loads',
            match,
            options: {
              limit: parseInt(req.query.limit),
              skip: parseInt(req.query.offset),
            },
          })
          .execPopulate();
      res.status(200).json({loads: loads});
    } catch (e) {
      return res.status(400).json({message: e.message});
    }
  } else {
    return res.status(400).json({message: 'You are not shipper!'});
  }
};

const getLoadById = async (req, res) => {
  const user = await User.findOne({_id: req.user._id});

  if (!user) {
    return res.status(400).json({message: 'User are not found!'});
  }

  if (user.role === 'SHIPPER') {
    try {
      const _id = req.params.id;
      const load = await Load.findOne({_id, created_by: req.user._id});

      if (!load) {
        return res.status(400).json({message: 'No such load'});
      }
      res.status(200).json({load: load});
    } catch (e) {
      return res.status(400).json({message: e.message});
    }
  } else {
    return res.status(400).json({message: 'You are not shipper!'});
  }
};

const iterateLoadState = async (req, res) => {
  const user = await User.findOne({_id: req.user._id});

  if (!user) {
    return res.status(400).json({message: 'User are not found!'});
  }

  if (user.role === 'DRIVER') {
    const trucks = await Truck.find({created_by: user._id});
    const truck = trucks.find((truck) => (truck.assigned_to = user._id));
    const load = await Load.findOne({assigned_to: truck._id});
    if (!load) {
      return res.status(400).json('You have not active loads');
    }
    let message;
    if (load.state === 'En route to delivery') {
      message = 'Arrived to delivery';
      load.status = 'SHIPPED';
      load.state = message;
      load.logs.push({message: `Load is ${message}`, time: Date.now()});
      await load.save();
    } else if (load.state === 'En route to Pick Up') {
      message = 'Arrived to Pick Up';
      load.state = message;
      load.logs.push({message: `Load is ${message}`, time: Date.now()});
      await load.save();
    } else if (load.state === 'Arrived to Pick Up') {
      message = 'En route to delivery';
      load.state = message;
      load.logs.push({message: `Load is ${message}`, time: Date.now()});
      await load.save();
    }
    return res.status(200).json({message: `Load state changed to ${message}`});
  } else {
    return res.status(400).json({message: 'You are not driver!'});
  }
};

const updateLoad = async (req, res) => {
  const user = await User.findOne({_id: req.user._id});

  if (!user) {
    return res.status(400).json({message: 'User are not found!'});
  }

  if (user.role === 'SHIPPER') {
    const updates = [];
    for (const value of Object.keys(req.body)) {
      if (typeof req.body[value] === 'object') {
        const res = Object.keys(req.body[value]);
        updates.push(...res);
      } else {
        updates.push(value);
      }
    }
    const allowedUpdates = [
      'name',
      'payload',
      'pickup_address',
      'delivery_address',
      'width',
      'length',
      'height',
    ];
    const isAllowed = updates.every((update) =>
      allowedUpdates.includes(update),
    );

    if (!isAllowed) {
      return res.status(400).send({error: 'Invalid updates!'});
    }

    try {
      const load = await Load.findOne({
        _id: req.params.id,
        created_by: req.user._id,
      });
      if (!load) {
        return res.status(400).json({message: 'No such load'});
      }
      if (load.status === 'NEW') {
        updates.forEach((update) => {
          if (
            update === 'width' ||
            update === 'length' ||
            update === 'height'
          ) {
            load.dimensions[update] = req.body.dimensions[update];
          } else {
            load[update] = req.body[update];
          }
        });
        await load.save();
        return res
            .status(200)
            .json({message: 'Load details changed successfully'});
      } else {
        return res.status(400).json({
          message: 'You can"t update info about this load.It is in progress',
        });
      }
    } catch (e) {
      return res.status(400).json({message: e.message});
    }
  } else {
    return res.send(400).json({message: 'You are not shipper!'});
  }
};

const deleteLoad = async (req, res) => {
  const user = await User.findOne({_id: req.user._id});

  if (!user) {
    return res.status(400).json({message: 'User are not found!'});
  }

  if (user.role === 'SHIPPER') {
    const load = await Load.findOne({
      _id: req.params.id,
      created_by: req.user._id,
    });
    if (!load) {
      return res.status(404).json({message: 'No such load'});
    }
    if (load.status === 'NEW') {
      await load.delete();
      return res.status(200).json({message: 'Load deleted successfully'});
    } else {
      return res.status(400).json({
        message: 'You can"t delete info about this load.It is in progress',
      });
    }
  } else {
    return res.send(400).json({message: 'You are not shipper!'});
  }
};

const postLoad = async (req, res) => {
  const user = await User.findOne({_id: req.user._id});

  if (!user) {
    return res.status(400).json({message: 'User are not found!'});
  }

  if (user.role === 'SHIPPER') {
    const _id = req.params.id;
    const load = await Load.findOne({_id, created_by: user._id});

    if (!load) {
      return res.status(400).json({message: 'No such load'});
    }
    const trucks = await Truck.find();

    const suitableTruck = trucks.find((truck) => truck.assigned_to !== null);
    if (suitableTruck) {
      load.status = 'ASSIGNED';
      load.assigned_to = suitableTruck._id;
      load.logs.push({
        message: `Load assigned to driver with id ${suitableTruck.assigned_to}`,
        time: Date.now(),
      });
      await load.save();
      return res.status(200).json({
        message: 'Load posted successfully',
        driver_found: true,
      });
    } else {
      return res.status(400).json({message: 'No suitable truck for you now'});
    }
  } else {
    return res.send(400).json({message: 'You are not shipper!'});
  }
};

const getShippingInfo = async (req, res) => {
  const user = await User.findOne({_id: req.user._id});

  if (!user) {
    return res.status(400).json({message: 'User are not found!'});
  }

  if (user.role === 'SHIPPER') {
    const _id = req.params.id;
    const load = await Load.findOne({_id, created_by: user._id});

    if (!load) {
      return res.status(400).json({message: 'No such load'});
    }

    const truck = await Truck.findOne({_id: load.assigned_to});

    res.status(200).json({load: load, truck: truck});
  } else {
    return res.status(400).json({message: 'You are not shipper!'});
  }
};

module.exports = {
  createLoad,
  getLoads,
  getLoadById,
  iterateLoadState,
  updateLoad,
  deleteLoad,
  postLoad,
  getShippingInfo,
};
