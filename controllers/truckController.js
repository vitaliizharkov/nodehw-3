const {Truck} = require('../models/truckModel');
const {User} = require('../models/userModel');

const getTruck = async (req, res) => {
  const user = await User.findOne({_id: req.user._id});

  if (!user) {
    return res.status(400).json({message: 'User are not found!'});
  }

  if (user.role === 'DRIVER') {
    const trucks = await Truck.find({created_by: req.user._id});

    if (!trucks) {
      return res.status(400).json({message: 'Truck not found!'});
    }

    return res.status(200).json({trucks})
  } else {
    return res.status(400).json({message: 'You are not driver'});
  }
};

const addTruck = async (req, res) => {
  const user = await User.findOne({_id: req.user._id});

  if (!user) {
    return res.status(400).json({message: 'User are not found!'});
  }

  if (user.role === 'DRIVER') {
    const truck = new Truck({
      created_by: req.user._id,
      type: req.body.type,
    });
    try {
      await truck.save();
      return res.status(200).json({message: 'Truck created successfully'});
    } catch (e) {
      return res.status(400).json({message: e.message});
    }
  } else {
    return res.status(400).json({message: 'You are not driver'});
  }
};

const getTruckById = async (req, res) => {
  const user = await User.findOne({_id: req.user._id});

  if (!user) {
    return res.status(400).json({message: 'User are not found!'});
  }

  if (user.role === 'DRIVER') {
    const _id = req.params.id;

    const truck = await Truck.findOne({_id, created_by: req.user._id});

    if (!truck) {
      return res.status(400).json({message: 'No such truck'});
    }
    if (!truck.assigned_to) {
      return res.status(200).json({truck: truck});
    } else {
      return res.status(400).json({message: 'This truck is not assigned to you!'});
    }
  } else {
    return res.status(400).json({message: 'You are not driver'});
  }
};

const updateTruckById = async (req, res) => {
  const user = await User.findOne({_id: req.user._id});

  if (!user) {
    return res.status(400).json({message: 'User are not found!'});
  }

  if (user.role === 'DRIVER') {
    const _id = req.params.id;

    const truck = await Truck.findOne({_id, created_by: req.user._id});

    if (!truck) {
      return res.status(400).json({message: 'No such truck'});
    }
    if (!truck.assigned_to) {
      try {
        truck.type = req.body.type;
        await truck.save();
        return res
            .status(200)
            .json({message: 'Truck details changed successfully'});
      } catch (e) {
        return res.status(400).json({message: e.message});
      }
    } else {
      return res.status(400).json({
        message: 'You can"t edit truck info while ou truck is assigned',
      });
    }
  } else {
    return res.status(400).json({message: 'You are not driver'});
  }
};

const deleteTruck = async (req, res) => {
  const user = await User.findOne({_id: req.user._id});

  if (!user) {
    return res.status(400).json({message: 'User are not found!'});
  }

  if (user.role === 'DRIVER') {
    const _id = req.params.id;

    const truck = await Truck.findOne({_id, created_by: req.user._id});

    if (!truck) {
      return res.status(400).json({message: 'No such truck'});
    }
    if (!truck.assigned_to) {
      try {
        await Truck.findOneAndDelete({
          _id,
          created_by: req.user._id,
        });
        return res.status(200).json({message: 'Truck deleted successfully'});
      } catch (e) {
        return res.status(400).json({message: e.message});
      }
    } else {
      return res.status(400).json({
        message: 'You can"t delete truck while ou truck is assigned',
      });
    }
  } else {
    return res.status(400).json({message: 'You are not driver'});
  }
};

const assignTruck = async (req, res) => {
  const user = await User.findOne({_id: req.user._id});

  if (!user) {
    return res.status(400).json({message: 'User are not found!'});
  }

  if (user.role === 'DRIVER') {
    const _id = req.params.id;

    const truck = await Truck.findOne({_id, created_by: req.user._id});

    if (!truck) {
      return res.status(400).json({message: 'No such truck'});
    }
    try {
      truck.assigned_to = req.user._id;
      await truck.save();
      return res.status(200).json({message: 'Truck assigned successfully'});
    } catch (e) {
      return res.status(400).json({message: e.message});
    }
  } else {
    res.status(400).json({message: 'You are not driver'});
  }
};

module.exports = {
  getTruck,
  getTruckById,
  deleteTruck,
  assignTruck,
  updateTruckById,
  addTruck,
};
