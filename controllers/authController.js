const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const dotenv = require('dotenv');
dotenv.config();
const {User} = require('../models/userModel');

module.exports.registration = async (req, res) => {
  const {email, password, role} = req.body;

  if (!email) {
    return res.status(400).json({message: 'Enter valid email'});
  }
  if (!password) {
    return res.status(400).json({message: 'Enter valid password'});
  }

  if (!role) {
    return res.status(400).json({message: 'Choose valid role'});
  }

  const userExists = await User.findOne({email: email});

  if (userExists) {
    return res.status(400).json({message: 'This user already exists!'});
  }

  const user = new User({
    email,
    password: await bcrypt.hash(password, 10),
    role,
  });

  await user.save();

  res.json({message: 'User created successfully!'});
};

module.exports.login = async (req, res) => {
  const {email, password} = req.body;

  const user = await User.findOne({email});

  if (!user) {
    return res.status(400).json({message: `No user with 
    email '${email}' found!`});
  }

  if ( !(await bcrypt.compare(password, user.password)) ) {
    return res.status(400).json({message: `Wrong password!`});
  }

  const token = jwt.sign({email: user.email,
    _id: user._id, role: user.role}, process.env.JWT_SECRET);
  res.json({message: 'Success', jwt_token: token});
};
