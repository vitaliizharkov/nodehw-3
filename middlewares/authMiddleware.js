const jwt = require('jsonwebtoken');
const dotenv = require('dotenv');
dotenv.config();

module.exports.authMiddleware = (req, res, next) => {
  const header = req.headers['authorization'];

  if (!header) {
    return res.status(401).json({
      message: `Authorization http header was not found`,
    });
  }

  const token = header.split(' ')[1];

  if (!token) {
    return res.status(401).json({message: `JWT token was not found`});
  }

  req.user = jwt.verify(token, process.env.JWT_SECRET);
  next();
};
