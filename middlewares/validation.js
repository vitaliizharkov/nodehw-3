const Joi = require('joi');

module.exports.validateRegistration = async (req, res, next) => {
  const schema = Joi.object({
    email: Joi.string().email({minDomainSegments: 2}),

    password: Joi.string()
        .required(),

    role: Joi.string().valid('DRIVER', 'SHIPPER').required(),
  });

  const validation = await schema.validateAsync(req.body);
  const {error} = validation;
  if (error) {
    return res.status(400).json({message: error.message});
  }
  next();
};
