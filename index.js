const express = require('express');
const app = express();
const dotenv = require('dotenv');
dotenv.config();
const port = process.env.PORT || 8080;
const morgan = require('morgan');
const mongoose = require('mongoose');
const authRouter = require('./routers/authRouter');
const loadRouter = require('./routers/loadRouter');
const userRouter = require('./routers/userRouter');
const truckRouter = require('./routers/truckRouter');

app.use(express.json());
app.use(express.static('build'));
app.use(morgan('tiny'));

app.use('/api/auth', authRouter);
app.use('/api/loads', loadRouter);
app.use('/api/users', userRouter);
app.use('/api/trucks', truckRouter);

app.use((err, req, res, next) => {
  return res.status(500).json({message: err.message});
});

const start = async () => {
  await mongoose.connect(process.env.DB_CONNECT, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true,
  });

  app.listen(port, () => {
    console.log(`Server started at: http://localhost:${port}`);
  });
};

start();
